# fuxico

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Docker

Instalando o docker: [[https://docs.docker.com/install/]]
Para executar: [[https://gitlab.com/marialab/fuxico/fuxico-image/blob/8c8c000875b9dc10269f0927c9792d224f5260a9/README.md]]

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

